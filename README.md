# Tópicos importantes

```
Redes multimidia - conceitual(sem contas) (aula ??)
Correcao de erro (aula ??)
Questao 4 e 16 da lista
```
- Aula 14 (ok)
    - Modelos de sincronizacao
    - Relações temporais de allen
- Aula 15 (ok)
    - ginga
        - como ele mudou o cenario de middleware
- Aula 16 (ok)
    - ncl
        - apenas conceitual(oq é um conector, um descritor, etc)
- Aula 17 (ok)
    - Hipermidia na web
        - qual vantagem de usar ncl no lugar de smil ou html
- Aula 18
    - questao 4 da lista
    - recuperacao de perda (ok)
    - Protocolos rtp/rtsp (ok)
- Aula 19
    - não pretende cobrar os protocolos pois ta mais relacionado a redes
    - sip
- Aula 20
    - tudo importante 
    - Tipos de prioridades de fila

-----------

# Aula 14

## Modelos de sincronização

### Unidades de Tempo

![](./assets/unidades_tempo_sincronizacao.png)

- Instante
    - Evento acontece instantaneamente e não tem duração
- Intervalo
    - Evento acontece entre um par de instantes e tem duração

#### Relações temporais

##### Entre instantes

![](./assets/relacoes_temporais_instantes.png)

##### Entre intervalos (Allen 83)

![](./assets/relacoes_temporais_intervalos.png)

### Paradigmas de sincronização temporal

- Scripts
    - Usa programação baseada em scripts para especificar a sincronização
    - ex: html5, js, flash
    - vantagens: expressividade
    - desvantagens: precisa saber programar e é difícil visualizar
- Timeline
    - Objetos são posicionados no instante de tempo que devem começar a serem exibidos
    - ex: adobe premier, windows video maker, apple final cut
    - vantagens: intuitivo, facil de visualizar
    - desvantagens: sem relação entre objetos, "dificil de editar", sem interatividade, sem ajustes temporais durante exec
- Ferramentas para Especificação Formal
    - ![](./assets/ferramentas_espec_formal.png)
    - fluxogramas, redes de petri
    - ex: macromedia authorware
    - vantagens: expressividade, ajustes durante exec, verificação formal
    - desvantagens: precisa saber formalismo, redes podem ficar complexas
- Sincronização Hierárquica
    - ![](./assets/sincronizacao_hierarquica.png)
    - Composições com semântica temporal
        - sequencial e paralela
    - ex: XMT, SMIL, CMIF, AHM
    - vantagens: facil autoria, expressividade, ajustes durante exec
    - desvantagens: sem interatividade, dificil sincronizar partes de nós
        - Para sincronizar com partes de nós, precisamos:
            - Dividir o objeto em várias partes ou 
            - Combinar esse modelo com outra abordagem (baseada em eventos, por exemplo)
- Baseado em Restrições
    - Conjunto básico de relações entre instantes ou intervalos (relações de Allen)
    - ex: FireFly, Madeus
    - vantagens: facil autoria, expressividade, ajustes durante exec
    - desvantagens: sem interatividade, dificil sincronizar partes de nós
        - Para sincronizar com partes de nós, precisamos:
            - Dividir o objeto em várias partes ou 
            - Combinar esse modelo com outra abordagem (baseada em eventos, por exemplo)
- Baseado em Eventos
    - Eventos são representados como instantes ou intervalos de tempo
    - Relacionamentos são definidos baseados em eventos que ocorrem durante a apresentação do documento
        - Apresentação de uma âncora
        - Seleção de uma âncora (clique do mouse)
        - Atribuição (mudança de valores de atributos)
        - Posicionamento do mouse sobre uma âncora
    - ex: NCL, NCM, MHEG, IMAP, Labyrinth
    - vantagens: expressividade, facil de tratar interatividade, ajustes durante exec
    - desvantagens: dificuldade de autoria

# Aula 15

## Sistema brasileiro de TV (SBTVD)

![](./assets/arq_sbtvd.png)

## Middleware

Middleware é uma camada de software adicional, cujo **objetivo é oferecer um serviço padronizado às aplicações, 
escondendo as especificidades e heterogeneidades das camadas de hardware e sistema operacional**, dando suporte 
às facilidades básicas de codificação, transporte e modulação  de um sistema de televisão digital.

Requisitos de um middleware:
- Suporte à sincronização e interatividade
    - Suporte a canal de retorno
- Suporte a múltiplos dispositivos
- Suporte a adaptação do conteúdo e da apresentação
- Suporte à edição ao vivo

![](./assets/middleware_api.png)

### Ginga

Pode ser dividido em dois subsistemas principais, que permitem o desenvolvimento de aplicações seguindo dois paradigmas de programação diferentes.
- GINGA J: aplicações procedurais Java Xlets
- GINGA NCL: aplicações declarativas NCL e Lua

![](./assets/arq_ginga.png)

Features GINGA NCL:
- Linguagem NCL Nested Context Language
- Suporte à sincronização e interatividade
    - Suporte a canal de retorno
- Suporte a múltiplos dispositivos
- Suporte a adaptação do conteúdo e da apresentação
- Suporte à edição ao vivo

> Como mudou o cenário? (minha visão)
- Uso de tecnologias modernas para encode/decode == +qualidade
    - H.264 (video)
    - MPEG-4 (audio)
    - MPEG2-TS (transporte)
    - BST-OFDM (modulação)
    - é programável e permite interatividade (NCL)
    - permite que um único sinal possa ser exibido de forma diferente entre dispositivos diferentes
        - one-seg (dispositivos portáteis)
        - full-seg (dispositivos não portáteis)
    - nova versão do ginga (dtv play) permite interagir com smart tvs e webservices

# Aula 16

## NCL

- É necessário um modelo conceitual hipermídia para expressar os componentes de um documento
- Modelo NCM Nested Context Model
    - é usado pela linguagem NCL Nested Context Language
- Componentes de um documento são representados pelas entidades do modelo

### NCM

Principais entidades hipermídia:
- Blocos de informação (texto, audio, video, imagem ...)
    - Nós (nodes)
        - Ancoras (anchors)
- Relacionamentos
    - elos (links)
    - nós de composição/composições/contextos
        - grupos de nós e de elos


> Conectores

Elos divididos entre dois elementos
![](./assets/conector.png)

- Definição da relação -> conector hipermídia
- Definição dos participantes -> conjunto de binds

Papeis de um conector:
- Modelo de sincronização baseado em eventos
- Ponto de interface de um conector (papel) especifica o comportamento de um participante da relação
- Papéis são definidos baseados em uma máquina de estado de
evento:

> Descritores

Indicam as características de exibição de um nó, como volume, duração, região e index de foco (botão)

# Aula 17

Qual vantagem de usar NCL no lugar de SMIL ou HTML?

## HTML

- Não foi elaborado para ser usado especificamente no contexto de multimídia/hipermídia
- Suporte a sincronização é "recente" e requer o uso de JavaScript, além de HTML
- Uso de elos é bem limitado
    - Não dá para reutilizar conteúdo
    - Só podem ser percorridos em uma direção
    - Elos só são disparados com interação do usuário

## SMIL

- A estrutura do documento é a própria estrutura de apresentação
    - Inclusão de novos relacionamentos temporais pode exigir uma reestruturação de todo o documento
    - ex: importar um componente que é reutilizado em vários projetos (entendi isso)
- Não tem reúso de nós e composições
- Ativação de elos ocorre apenas com interação de usuário
- Descontinuado em prol do HTML5

# Aula 18

## Protocolo RTSP (Real Time Streaming Protocol)

- Protocolo de aplicação do tipo cliente servidor.
- Permite ao usuário controlar apresentações de mídia contínua
    - voltar ao início, avançar, pausa, continuar, seleção de trilha, etc...
- O que ele não faz
    - não define como o aúdio e o vídeo é encapsulado para transmissão sobre a rede
    - não restringe como a mídia contínua é transportada: pode usar UDP ou TCP
    - não especifica como o receptor armazena o aúdio e o vídeo
- Mensagens de controle são enviadas "fora da banda" (igual FTP)
    - ex: fluxo de dados de mídia são transmitidos de forma separada/conexão separada

![](./assets/rtsp.png)

## Protocolo RTP (Real Time Protocol)

- protocolo (de aplicação) para transportar dados de audio e vídeo
- oferece:
    - identificação do tipo de carga
    - numeração da sequência de pacotes
    - marcas de tempo
- roda em cima do udp
- permite trocar de codificação em tempo real
- cada fonte (camera, microfone etc) pode ter um fluxo de pacotes RTP independentes

### RTCP (Real Time Control Protocol)

- Trabalha junto com o RTP
- Cada participante de uma sessão RTP transmite pacotes de controle RTCP para outros participantes
    - cada pacote RTCP contém dados do trasnfmissão e/ou receptor (métricas)
    - essas metricas podem ser usadas para controle de desempenho e/ou diagnostico

## Recuperacao de perda

### Forward error correction (FEC): esquema simples

Resumo (minha visão):
- dado um grupo de n blocos, cria um bloco reduntante usando XOR nos blocos originais
- permite reconstruir um bloco, caso APENAS UM bloco tenha sido perdido

### Forward error correction (FEC): segundo esquema (sem nome)

Resumo (minha visão):
- a cada transmissão de pacote, além do dado do pacote atual, é enviado dados de baixa qualidade do pacote anterior
- se um pacote x for perdido, é possível utilizar o pacote x+1 para reproduzir o pacote perdido (em qualidade inferior)

![](./assets/fec2.png)

### Intercalamento

Resumo (minha visão):
- intercala blocos de pacotes, assim, caso um pacote seja perdido, nem todos os dados do pacote original serão perdidos
- blocos intercalados são remontados no receptor
- se um pacote for perdido, ainda será possível reproduzir, mesmo que com uma qualidade inferior

![](./assets/intercalamento.png)

### Recuperação pelo receptor de fluxos de aúdio danificados

Resumo (minha visão):
- o receptor tentar produzir um pacote perdido baseado em algo
    - sugerido pela video aula: repetir o pacote anterior

# Aula 19

## SIP (Session Initiation Protocol)

- Protocolo de sinalização para estabelecimento/encerramento de sessões em uma rede IP
    - sessão: chamada telefônica ou conferência multimídia colaborativa
- Protocolo textual que usa o paradigma cliente-servidor no nível de aplicação
    - Pode usar UDP ou TCP
- exemplo de aplicação: VoIP

Características principais:
- Suporta troca de capacidades entre terminais de usuários
- Suporta mobilidade dos usuários através de proxies e redirecionamento de chamadas
    - Redirect e fork
- Conferências multicast
- Autenticação dos usuários